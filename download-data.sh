#!/usr/bin/env bash
################################################################
# Script to download data for 
# the AI4EOSC tutorial during the EGI2023 conference
# * example dataset
# * pre-trained model
################################################################

# Download example dataset
echo "Downloading dataset, submarine_det.zip"
curl --insecure -o submarine_det.zip https://data-deep.a.incd.pt/index.php/s/yx97Ny3Df4BbiGw/download/submarine_det.zip


# pre-trained model, ca. 1.5GB (!)
echo "Downloading pre-trained model, 2023-05-10_121810.zip"
curl --insecure -o 2023-05-10_121810.zip \
    https://data-deep.a.incd.pt/index.php/s/4q49kPP6DyMW23P/download/2023-05-10_121810.zip
