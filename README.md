# ai4eosc-egi23-tutorial

## Info

This repository contains material (e.g. Jupyter Notebook) for the AI4EOSC Tutorial given during the [EGI2023 Conference](https://www.egi.eu/event/egi2023/).

## Preparation Steps

### On the AI4EOSC | iMagine platform
* Obtain account for either [AI4EOSC DASHBOARD](https://dashboard.cloud.ai4eosc.eu/) or [iMagine DASHBOARD](https://dashboard.cloud.imagine-ai.eu/) or [Tutorials DASHBOARD](https://tutorials.cloud.ai4eosc.eu/)
* Deploy "AI4OS Development Environment
  * provide “Deployment title”, e.g. “dev jupyter”
  * “Service to run”: “Jupyter”
  * provide “Password” : at least 9 characters!
  * “Docker tag” : “pytorch1.13”
* In the AI4OS Development Environment, you may need to run in the JupyterLab terminal:
  * `apt update`
  * `apt install -y unzip`
  * `apt install -y gcc`
  * `apt install -y libgl1`
* To ownload provided Notebook and Upload inside the AI4OS Development Environment, execute the Notebook

### If working locally
You may also configure your local machine to run the notebook, you will need:
* docker, e.g. `sudo apt install docker-ce`
* docker-compose, e.g. `sudo apt install docker-compose-plugin`
* git, e.g. `sudo apt install git`
* configure virtual environment, e.g.:
  * `sudo apt install python3-virtualenv`
  * `python3 -m venv path/to/egi-venv`
  * `source egi-venv/bin/activate`
  * (later `deactivate`)
* install jupyterlab in the virtual environment: `pip3 install jupyterlab`
* run JupyterLab in the dedicated directory: `jupyter lab`

### Download the notebook and the data
* In the JupyterLab either on the cloud platform or locally, open Terminal
* in the Teminal, execute `git clone https://git.scc.kit.edu/m-team/ai/ai4eosc-egi23-tutorial`
* go in the ai4eosc-egi23-tutorial directory, run the script `./download-data.sh` in order to download the example dataset and pre-trained model

## Tutorial
==>> Follow the notebook instructions
